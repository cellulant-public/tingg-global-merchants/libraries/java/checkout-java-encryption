package io.cellulant.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.cellulant.model.Payload;

/**
 * The Class CheckoutEncryption.
 * @author Rakesh.Soni
 */
public class CheckoutEncryption {

    // private String accessKey;	
	
    /** The iv key. */
    private String ivKey;
    
    /** The secret key. */
    private String secretKey;
    
    /** The encryption. */
    private Encryption encryption; 
    
    /** The debug. */
    private boolean debug = true;
	
	/**
	 * Constructor
	 * Instantiates a new checkout encryption.
	 *
	 * @param ivKey the iv key
	 * @param secretKey the secret key
	 */
	public CheckoutEncryption(String ivKey, String secretKey){
		// this.accessKey = accessKey;
		this.ivKey = ivKey;
		this.secretKey = secretKey;		
		this.encryption = new Encryption(this.ivKey, this.secretKey);		
	}
	
	
    /**
     * Encrypt the payload object
     * 
     * @param payload the payload
     * @return the string
     * @throws Exception the exception
     */
    public String encrypt(Payload payload) throws Exception{
        ObjectMapper objMapper = new ObjectMapper();  
        try {
            // Converting the Java object into a JSON string  
            String jsonPayload = objMapper.writeValueAsString(payload);  
            return encrypt(jsonPayload);
        } catch (Exception e) {
        	System.err.println("Exception in encrypting payload " + e);            
            throw e;
        }
    }

    /**
     * Encrypt the JSON String
     *
     * @param jsonPayload the json payload
     * @return the string
     * @throws Exception the exception
     */
    public String encrypt(String jsonPayload) throws Exception{        
        try {
            String encryptedPayload = encryption.encrypt(jsonPayload);
            if(debug) {
            	System.out.println("JSON =>" + jsonPayload);	
            	System.out.println("Encrypted Payload =>" + encryptedPayload);	
            }
            // return new CheckoutResponse(params, accessKey, payload.getCountryCode());
            return encryptedPayload;
        } catch (Exception e) {
        	System.err.println("Exception in encrypting payload " + e);            
            throw e;
        }
    }    
    
}



