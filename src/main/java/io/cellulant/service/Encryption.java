package io.cellulant.service;

import java.util.Base64;
import javax.crypto.Cipher;
import java.security.MessageDigest;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import javax.crypto.spec.IvParameterSpec;

class Encryption {

    private String ivKey;
    private String secretKey;

    Encryption(String ivKey, String secretKey) {
        this.ivKey = ivKey;
        this.secretKey = secretKey;
    }

    /**
     * encrypt - a function to encrypt your string using AES 128 it encrypts to
     * AES 128 if we specify no padding to be done
     *
     * @param plainText String
     * @return String
     * @throws Exception
     */
    String encrypt(String plainText) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hashedIV = digest.digest(this.ivKey.getBytes(StandardCharsets.UTF_8));
        byte[] hashedSecret = digest.digest(this.secretKey.getBytes(StandardCharsets.UTF_8));

        IvParameterSpec IV = new IvParameterSpec(
                bytesToHex(hashedIV).substring(0, 16).getBytes()
        );

        SecretKeySpec secret = new SecretKeySpec(
                bytesToHex(hashedSecret).substring(0, 32).getBytes(),
                "AES"
        );

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secret, IV);
        byte[] encrypted = cipher.doFinal(plainText.getBytes());

        String params = Base64.getEncoder().encodeToString(encrypted);
        return Base64.getEncoder().encodeToString(params.getBytes());
    }

    /**
     * bytesToHex - a function to convert encrypted byte to a hexadecimal String
     *
     * @param data byte[]
     * @return string String
     */
    static String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        }

        int len = data.length;
        String string = "";
        for (int i = 0; i < len; i++) {
            if ((data[i] & 0xFF) < 16) {
                string = string + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
            } else {
                string = string + java.lang.Integer.toHexString(data[i] & 0xFF);
            }
        }
        return string;
    }

}

