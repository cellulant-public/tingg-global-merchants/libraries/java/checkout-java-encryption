package io.cellulant.model;

//Checkout Response class
public class CheckoutResponse {

  private String params;
  private String accessKey;
  private String countryCode;

  public CheckoutResponse(String params, String accessKey, String countryCode) {
      this.params = params;
      this.accessKey = accessKey;
      this.countryCode = countryCode;
  }

	public String getParams() {
		return params;
	}
	
	public void setParams(String params) {
		this.params = params;
	}
	
	public String getAccessKey() {
		return accessKey;
	}
	
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
  
  
}