package io.cellulant.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

/** @author rakesh.soni */
public class Payload {

	  @JsonProperty("merchant_transaction_id")
	  private String merchantTransactionId;
	
	  @JsonProperty("customer_first_name")
	  private String customerFirstName;
	
	  @JsonProperty("customer_last_name")
	  private String customerLastName;
	
	  @JsonProperty("msisdn")
	  private long msisdn;
	
	  @JsonProperty("customer_email")
	  private String customerEmail;
	
	  @JsonProperty("request_amount")
	  private double requestAmount;
	
	  // Currency Iso code
	  @JsonProperty("currency_code")
	  private String currencyCode;
	  
	  // This is the account reference number for the customer
	  @JsonProperty("account_number")
	  private String accountNumber;
	
	  @JsonProperty("service_code")
	  private String serviceCode;
	  
	  @JsonProperty("due_date")
	  private String dueDate;
	
	  @JsonProperty("request_description")
	  private String requestDescription;
	
	  // Currency Iso code
	  @JsonProperty("country_code")
	  private String countryCode;  
	
	  // Optional 
	  @JsonProperty("language_code")
	  private String languageCode;
	  
	  //payerClientCode in 2.0 
	  @JsonProperty("payment_option_code")
	  private String paymentOptionCode ;  
	  
	  @JsonProperty("success_redirect_url")
	  private String successRedirectUrl;
	  
	  @JsonProperty("fail_redirect_url")
	  private String failRedirectUrl;
	
	  // 2.0 
	  @JsonProperty("pending_redirect_url")
	  private String pendingRedirectUrl;   
	  
	  // 2.0
	  // private String paymentWebhookUrl;  
	  
	  @JsonProperty("callback_url")
	  private String callbackUrl;  
	  
	  // @JsonProperty("channel_origin")
	  // private String channelOrigin;
	  
	  // @JsonProperty("encrypted_payload")
	  // public String encryptedPayload;
	  
	  // @JsonProperty("access_key")
	  // public String accessKey;
	  
	  // @JsonProperty("is_offline")
	  // public boolean isOffline;
	  
	  @JsonProperty("charge_beneficiaries")
	  private ArrayList<ChargeBenificiaryDetails> chargeBeneficiaries;
  
	  // No Args Constructor 
	  public Payload() {
	  	
	  }
  
	  // All Args constructor 
	  public Payload(String merchantTransactionID, String customerFirstName, String customerLastName, long MSISDN, String customerEmail, 
			  Double requestAmount, String currencyCode, String accountNumber, String serviceCode, String dueDate, 
			  String requestDescription, String countryCode, String languageCode, String paymentOptionCode, 
			  String successRedirectUrl, String failRedirectUrl, String pendingRedirectUrl, String callbackUrl
			  ) {
	  
		      this.merchantTransactionId = merchantTransactionID;
		      this.customerFirstName = customerFirstName;
		      this.customerLastName = customerLastName;
		      this.msisdn = MSISDN;
		      this.customerEmail = customerEmail;
		      this.requestAmount = requestAmount;
		      this.currencyCode = currencyCode;
		      this.accountNumber = accountNumber;
		      this.serviceCode = serviceCode;
		      this.dueDate = dueDate;
		      this.requestDescription = requestDescription;
		      this.countryCode = countryCode;
		      this.languageCode = languageCode;
		      this.paymentOptionCode = paymentOptionCode;	      
		      this.successRedirectUrl = successRedirectUrl;
		      this.failRedirectUrl = failRedirectUrl;
		      this.pendingRedirectUrl = pendingRedirectUrl;
		      // this.paymentWebhookUrl = paymentWebhookUrl;
		      this.callbackUrl = callbackUrl;      
	     
	  	}

		public String getMerchantTransactionId() {
			return merchantTransactionId;
		}
	
		public void setMerchantTransactionId(String merchantTransactionId) {
			this.merchantTransactionId = merchantTransactionId;
		}
	
		public String getCustomerFirstName() {
			return customerFirstName;
		}
	
		public void setCustomerFirstName(String customerFirstName) {
			this.customerFirstName = customerFirstName;
		}
	
		public String getCustomerLastName() {
			return customerLastName;
		}
	
		public void setCustomerLastName(String customerLastName) {
			this.customerLastName = customerLastName;
		}
	
		public long getMsisdn() {
			return msisdn;
		}
	
		public void setMsisdn(long msisdn) {
			this.msisdn = msisdn;
		}
	
		public String getCustomerEmail() {
			return customerEmail;
		}
	
		public void setCustomerEmail(String customerEmail) {
			this.customerEmail = customerEmail;
		}
	
		public double getRequestAmount() {
			return requestAmount;
		}
	
		public void setRequestAmount(double requestAmount) {
			this.requestAmount = requestAmount;
		}
	
		public String getCurrencyCode() {
			return currencyCode;
		}
	
		public void setCurrencyCode(String currencyCode) {
			this.currencyCode = currencyCode;
		}
	
		public String getAccountNumber() {
			return accountNumber;
		}
	
		public void setAccountNumber(String accountNumber) {
			this.accountNumber = accountNumber;
		}
	
		public String getServiceCode() {
			return serviceCode;
		}
	
		public void setServiceCode(String serviceCode) {
			this.serviceCode = serviceCode;
		}
	
		public String getDueDate() {
			return dueDate;
		}
	
		public void setDueDate(String dueDate) {
			this.dueDate = dueDate;
		}
	
		public String getRequestDescription() {
			return requestDescription;
		}
	
		public void setRequestDescription(String requestDescription) {
			this.requestDescription = requestDescription;
		}
	
		public String getCountryCode() {
			return countryCode;
		}
	
		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}
	
		public String getLanguageCode() {
			return languageCode;
		}
	
		public void setLanguageCode(String languageCode) {
			this.languageCode = languageCode;
		}
	
		public String getPaymentOptionCode() {
			return paymentOptionCode;
		}
	
		public void setPaymentOptionCode(String paymentOptionCode) {
			this.paymentOptionCode = paymentOptionCode;
		}
	
		public String getSuccessRedirectUrl() {
			return successRedirectUrl;
		}
	
		public void setSuccessRedirectUrl(String successRedirectUrl) {
			this.successRedirectUrl = successRedirectUrl;
		}
	
		public String getFailRedirectUrl() {
			return failRedirectUrl;
		}
	
		public void setFailRedirectUrl(String failRedirectUrl) {
			this.failRedirectUrl = failRedirectUrl;
		}
	
		public String getPendingRedirectUrl() {
			return pendingRedirectUrl;
		}
	
		public void setPendingRedirectUrl(String pendingRedirectUrl) {
			this.pendingRedirectUrl = pendingRedirectUrl;
		}
	
		public String getCallbackUrl() {
			return callbackUrl;
		}
	
		public void setCallbackUrl(String callbackUrl) {
			this.callbackUrl = callbackUrl;
		}

		public ArrayList<ChargeBenificiaryDetails> getChargeBeneficiaries() {
			return chargeBeneficiaries;
		}
	
		public void setChargeBeneficiaries(ArrayList<ChargeBenificiaryDetails> chargeBeneficiaries) {
			this.chargeBeneficiaries = chargeBeneficiaries;
		}
		
	    @Override    
	    public String toString() {    
	        return "\"Payload [merchantTransactionId ="+ merchantTransactionId 
	        		+ ", customerFirstName = " + customerFirstName 
	        		+ ", customerLastName = " + customerLastName 
	        		+ ", msisdn = " + msisdn 
	        		+ ", customerEmail = " + customerEmail 
	        		+ ", requestAmount = " + requestAmount 
	        		+ ", currencyCode = " + currencyCode 
	        		+ ", accountNumber = " + accountNumber 
	        		+ ", serviceCode = " + serviceCode 
	        		+ ", dueDate = " + dueDate 
	        		+ ", requestDescription = " + requestDescription 
	        		+ ", countryCode = " + countryCode 
	        		+ ", languageCode = " + languageCode 
	        		+ ", paymentOptionCode = " + paymentOptionCode 
	        		+ ", successRedirectUrl = " + successRedirectUrl 
	        		+ ", failRedirectUrl = " + failRedirectUrl 
	        		+ ", pendingRedirectUrl = " + pendingRedirectUrl 
	        		+ ", callbackUrl = " + callbackUrl 	        		
	        		+ "]";    
	    }  
	  
}
