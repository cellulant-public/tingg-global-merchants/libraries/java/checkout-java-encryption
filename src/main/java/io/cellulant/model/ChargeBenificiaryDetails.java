package io.cellulant.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChargeBenificiaryDetails {
	
		
	@JsonProperty("charge_beneficiary_code")
	private String chargeBeneficiaryCode;

	@JsonProperty("amount")
	private double amount;

	public String getChargeBeneficiaryCode() {
		return chargeBeneficiaryCode;
	}

	public void setChargeBeneficiaryCode(String chargeBeneficiaryCode) {
		this.chargeBeneficiaryCode = chargeBeneficiaryCode;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}	

}
